//
//  MeteoTableViewCell.swift
//  MeteoVIP
//
//  Created by Alvaro IDJOUBAR on 13/03/2020.
//  Copyright © 2020 Alvaro IDJOUBAR. All rights reserved.
//

import UIKit

struct MeteoTableViewCellViewModel {
    let temp: Float
    let description: String
}

class MeteoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    static let cellIdentifier: String = "MeteoTableViewCell"

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.backgroundColor = .clear
        self.contentView.backgroundColor = .white
        self.layer.shadowOpacity = 0.28
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowRadius = 2
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.masksToBounds = false
        self.selectionStyle = .none
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupCell(with viewModel: MeteoTableViewCellViewModel) {
        temperatureLabel.text = "\(viewModel.temp.description)°C"
        descriptionLabel.text = viewModel.description
    }
}
