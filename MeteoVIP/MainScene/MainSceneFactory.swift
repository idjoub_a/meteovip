//
//  MainSceneFactory.swift
//  MeteoVIP
//
//  Created by Alvaro IDJOUBAR on 12/03/2020.
//  Copyright © 2020 Alvaro IDJOUBAR. All rights reserved.
//

import UIKit

final class MainSceneFactory {
    static func createModule() -> UIViewController {
        
        let storyboard = UIStoryboard(name: "MainViewController", bundle: nil)
        guard let mainVC = storyboard.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController else { return UIViewController() }
        let router = MainRouter()
        let presenter = MainPresenter()
        let interactor = MainInteractor()
        let weatherWeeklyNetworkManager = WeatherNetworkManager()
        let worker = MainWorker(networkManager: weatherWeeklyNetworkManager)
        
        interactor.worker = worker
        interactor.presenter = presenter
        mainVC.interactor = interactor
        mainVC.router = router
        presenter.viewController = mainVC
        
        return mainVC
    }
}
