//
//  MainInteractor.swift
//  MeteoVIP
//
//  Created by Alvaro IDJOUBAR on 12/03/2020.
//  Copyright (c) 2020 Alvaro IDJOUBAR. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol MainInteractorInput {
    // Input - Worker Side
    var worker: MainWorkerProtocol?  { get set }
    func fetchForecastWeeklyInteractor(latitude: String, longitude: String)
    
    // Input - VC Side
}

protocol MainInteractorOutput {
    // Output - to Presenter
    var presenter: MainPresenterInput? { get set }
}

final class MainInteractor: MainInteractorOutput {
    // MainInteractorInput
    var presenter: MainPresenterInput?
    var worker: MainWorkerProtocol?
}

extension MainInteractor: MainInteractorInput {
    func fetchForecastWeeklyInteractor(latitude: String, longitude: String) {
        worker?.fetchForecastWeeklyWorker(latitude, longitude, completion: { [unowned self] (result) in
            switch result {
            case .failure(let error):
                self.presenter?.notifyError(error)
            case .success(let forecast):
                self.presenter?.notifySuccess(with: forecast)
            }
        })
    }
}
