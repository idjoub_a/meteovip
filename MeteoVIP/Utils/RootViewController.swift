//
//  RootViewController.swift
//  MeteoVIP
//
//  Created by Alvaro IDJOUBAR on 17/03/2020.
//  Copyright © 2020 Alvaro IDJOUBAR. All rights reserved.
//

import UIKit

class RootNavigationController: UINavigationController, UISearchBarDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupNavbar()
    }
    
    private func setupNavbar() {
        self.navigationBar.prefersLargeTitles = true
        
        if #available(iOS 13.0, *) {
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithOpaqueBackground()
            navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.backgroundColor = UIColor.init(red: 231.0/255, green: 76.0/255, blue: 60.0/255, alpha: 1.0)
            self.navigationBar.standardAppearance = navBarAppearance
            self.navigationBar.scrollEdgeAppearance = navBarAppearance
        }
        else {
            self.navigationBar.tintColor = .white
            self.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
            self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
            self.navigationBar.barTintColor = UIColor.init(red: 231.0/255, green: 76.0/255, blue: 60.0/255, alpha: 1.0)
        }
//        let activity = UIBarButtonItem(customView: self.activityIndicator)
//        /* --- TEST BUTTONS --- */
        let rewind = UIBarButtonItem(barButtonSystemItem: .rewind, target: self, action: nil)//#selector(cleanRealmDB))
        let refresh = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: nil)//#selector(launchRequest))
//        /*---               ---*/
        navigationItem.rightBarButtonItems = [rewind, refresh/*, activity*/]
        navigationItem.rightBarButtonItems?.forEach({$0.tintColor = .white})
    }
}
