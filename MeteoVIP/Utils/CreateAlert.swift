//
//  CreateAlert.swift
//  MeteoVIP
//
//  Created by Alvaro IDJOUBAR on 17/03/2020.
//  Copyright © 2020 Alvaro IDJOUBAR. All rights reserved.
//

import UIKit

final class CreateAlert {
    static func alertError(with title: String, in vc: UIViewController) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: nil, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Retry", style: .default, handler: nil)
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            vc.present(alert, animated: true, completion: nil)
        }
    }
}
