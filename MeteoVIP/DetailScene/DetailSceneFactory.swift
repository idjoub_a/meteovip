//
//  DetailSceneFactory.swift
//  MeteoVIP
//
//  Created by Alvaro IDJOUBAR on 16/03/2020.
//  Copyright © 2020 Alvaro IDJOUBAR. All rights reserved.
//

import UIKit

final class DetailSceneFactory {
    static func createModule() -> UIViewController {
        
        let storyboard = UIStoryboard(name: "DetailViewController", bundle: nil)
        guard let detailVC = storyboard.instantiateViewController(withIdentifier: "DetailViewController") as? DetailViewController else { return UIViewController() }
        //let router = MainRouter()
        let presenter = DetailPresenter()
        let interactor = DetailInteractor()
        let weatherWeeklyNetworkManager = WeatherNetworkManager()
        let worker = DetailWorker(networkManager: weatherWeeklyNetworkManager)
        
        interactor.worker = worker
        interactor.presenter = presenter
        detailVC.interactor = interactor
        presenter.viewController = detailVC
        //self.router = router
        
        return detailVC
    }
}
