//
//  WeatherEndPoint.swift
//  BeerList
//
//  Created by Alvaro IDJOUBAR on 14/02/2020.
//  Copyright © 2020 Alvaro IDJOUBAR. All rights reserved.
//

import Foundation

public enum WeatherEndPoint {
    case weeklyForecast(_ latitude: String, _ longitude: String)
//    case dailyForecast
}

// Postman TEST URL : https://api.punkapi.com/v2/beers?malt=extra_pale

extension WeatherEndPoint: EndPointType {

    var baseURL: URL {
        switch self {
        case .weeklyForecast:
            guard let url = URL(string: "http://api.openweathermap.org") else { fatalError("baseURL could not be configured.")}
            return url
        }
    }
    
    var path: String {
        switch self {
        case .weeklyForecast:
            return "/data/2.5/forecast/daily"
        }
    }
    
    var httpMethod: HTTPMethod {
        switch self {
        case .weeklyForecast:
            return .get
        }
    }
    
    var task: HTTPTask {
        switch self {
        case .weeklyForecast(let latitude, let longitude):
            return .requestParameters(bodyParameters: nil,
                                      bodyEncoding: .urlEncoding,
                                      urlParameters: ["lat":latitude,
                                                      "lon":longitude,
                                                      "cnt":"7",
                                                      "lang":"en",
                                                      "units": "metric",
                                                      "APPID":"edef4985c1e5777dee5ccc8f3ee062e0"])
        }
    }
    
    var headers: HTTPHeaders? {
        switch self {
        case .weeklyForecast:
            return nil
        }
    }
}

