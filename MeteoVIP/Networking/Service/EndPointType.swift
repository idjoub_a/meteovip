//
//  EndPointType.swift
//  BeerList
//
//  Created by Alvaro IDJOUBAR on 14/02/2020.
//  Copyright © 2020 Alvaro IDJOUBAR. All rights reserved.
//

import Foundation

protocol EndPointType {
    var baseURL: URL { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var task: HTTPTask { get }
    var headers: HTTPHeaders? { get }
}
