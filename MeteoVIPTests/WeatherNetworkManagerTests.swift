//
//  WeatherNetworkManagerTests.swift
//  MeteoVIPTests
//
//  Created by Alvaro IDJOUBAR on 22/03/2020.
//  Copyright © 2020 Alvaro IDJOUBAR. All rights reserved.
//

import XCTest
@testable import MeteoVIP

class WeatherNetworkManagerTests: XCTestCase {
    
    var weatherNetworkManager: WeatherNetworkManager!
    var latitude: String!
    var longitude: String!
    
    override func setUp() {
        super.setUp()
        weatherNetworkManager = WeatherNetworkManager()
        longitude = "2.333333"
        latitude = "48.866667"
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func test_retreiveSucess() {
        weatherNetworkManager.getModel(ofType: Main.Something.Response.Forecast.self,
                                       endPoint: .weeklyForecast(latitude, longitude)) { (result) in
                                        let value = try? result.get()
                                        XCTAssertNotNil(value)
                                        switch result {
                                        case .success(let forecast):
                                            XCTAssertTrue(!forecast.city.country.isEmpty)
                                            XCTAssertTrue(!forecast.city.name.isEmpty)
                                            XCTAssertTrue(!forecast.forecastDays.isEmpty)
                                            break
                                        default:
                                            break
                                        }
        }
    }
}
