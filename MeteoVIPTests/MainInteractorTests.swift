//
//  MainInteractorTests.swift
//  MeteoVIPTests
//
//  Created by Alvaro IDJOUBAR on 24/03/2020.
//  Copyright © 2020 Alvaro IDJOUBAR. All rights reserved.
//

import XCTest
@testable import MeteoVIP

class MainInteractorTests: XCTestCase {

    let weatherWeeklyNetworkManager = WeatherNetworkManager()
    var worker: MainWorker<WeatherNetworkManager>!
    var interactor: MainInteractor!
    var latitude: String!
    var longitude: String!
    
    override func setUp() {
        super.setUp()
        worker = MainWorker(networkManager: weatherWeeklyNetworkManager)
        longitude = "2.333333"
        latitude = "48.866667"
        interactor = MainInteractor()
    }

    override func tearDown() {
        super.tearDown()
    }

    func test_retreiveSucess() {
//        interactor.fetchForecastWeeklyInteractor(latitude: latitude, longitude: longitude)
    }
    
    private class WeatherNetworkManagerSpy: NetworkManager {
        var calledMethodeName: String?
        var getModelCount = 0
        var getModelCalled: Bool {
            return getModelCount > 0
        }

        // NetworkManager Protocol stubs
        let router = Router<WeatherEndPoint>()
        func getModel<T: Decodable>(ofType: T.Type, endPoint: WeatherEndPoint, completion: @escaping (Result<T, NetworkErrorResponse>) -> ()) {
            getModelCount += 1
            calledMethodeName = "getModel"
        }
    }
}
