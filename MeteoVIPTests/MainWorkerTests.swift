//
//  MainWorkerTests.swift
//  MeteoVIPTests
//
//  Created by Alvaro IDJOUBAR on 24/03/2020.
//  Copyright © 2020 Alvaro IDJOUBAR. All rights reserved.
//

import XCTest
@testable import MeteoVIP

class MainWorkerTests: XCTestCase {
    
    let weatherWeeklyNetworkManager = WeatherNetworkManager()
    var worker: MainWorker<WeatherNetworkManager>!
    var latitude: String!
    var longitude: String!
    
    override func setUp() {
        super.setUp()
        worker = MainWorker(networkManager: weatherWeeklyNetworkManager)
        longitude = "2.333333"
        latitude = "48.866667"
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func test_getModel_interactor_count() {
        let managerSpy = WeatherNetworkManagerSpy()
        let workerSpy = MainWorker(networkManager: managerSpy)
        workerSpy.fetchForecastWeeklyWorker(latitude, longitude, completion: { _ in })
        XCTAssertEqual(managerSpy.getModelCount, 1)
        XCTAssertEqual(managerSpy.calledMethodeName, "getModel")
    }
    
    func test_retreiveSucess() {
        worker.fetchForecastWeeklyWorker(latitude, longitude) { (result) in
            let value = try? result.get()
            XCTAssertNotNil(value)
            switch result {
            case .success(let forecast):
                XCTAssertTrue(!forecast.city.country.isEmpty)
                XCTAssertTrue(!forecast.city.name.isEmpty)
                XCTAssertTrue(!forecast.forecastDays.isEmpty)
                break
            default:
                break
            }
        }
    }
    
    private class WeatherNetworkManagerSpy: NetworkManager {
        var calledMethodeName: String?
        var getModelCount = 0
        var getModelCalled: Bool {
            return getModelCount > 0
        }

        // NetworkManager Protocol stubs
        let router = Router<WeatherEndPoint>()
        func getModel<T: Decodable>(ofType: T.Type, endPoint: WeatherEndPoint, completion: @escaping (Result<T, NetworkErrorResponse>) -> ()) {
            getModelCount += 1
            calledMethodeName = "getModel"
        }
    }
}
